local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity:set_drawn_in_y_order(true)
  entity.damage = 1
  entity.damage_type = "physical"
end


function entity:enable_collision()
  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "hero" and other:get_can_be_hurt() then
      local damage = entity.damage
      local damage_type = entity.damage_type
      entity:clear_collision_tests()
      if other.process_hit then
        other:process_hit({damage = damage, enemy = entity, type = type})
      else
        other:start_hurt(enemy, damage)
      end
    end
  end)
end

