return {
  portrait = {
    path = "portraits/test_man.png",
    position = "right",
    offset_x = 0,
    offset_y = 0,
  },
  vox_blips = {
    "vox_blips/bloop_1",
    "vox_blips/bloop_2",
    "vox_blips/bloop_3",
    "vox_blips/bloop_4",
  },
  vox_blips_frequency = 70,
  vox_barks = {
    "vox_barks/grunt_1",
    "vox_barks/grunt_2",
    "vox_barks/grunt_3",
  },
}