local builder_builder = {}

function builder_builder:new(game, config)
  local bar_builder = {}

  --CONSTANTS
  local max_amount_function = config.max_amount_function or (function() return game:get_max_life() end)
  local current_amount_function = config.current_amount_function or (function() return game:get_magic() end)
  local DRAW_RATIO = config.draw_ratio or 4
  local color = config.color or "red"
  local CHECK_FREQUENCY = config.check_frequency or 30

  function bar_builder:new(game, config)

    local bar = {}
    bar.dst_x, bar.dst_y = config.x, config.y
    bar.color = color
    local fade_on_suspend = config.fade_on_suspend

    local quest_width, quest_height = sol.video.get_quest_size()
    bar.surface = sol.surface.create(400, 8)
    bar.background = sol.sprite.create("hud/stat_bars/background")
    bar.foreground = sol.sprite.create("hud/stat_bars/" .. bar.color)
    bar.endcap = sol.sprite.create("hud/stat_bars/endcap")
    bar.amount_displayed = 0
    bar.width, bar.height = bar.background:get_size()

    function bar:check()
      local current_amount = current_amount_function()
      local need_rebuild = false

      if current_amount ~= bar.amount_displayed then
        need_rebuild = true
        local difference = current_amount - bar.amount_displayed
        if difference % 10000 == 0 then
          increment = 10000
        elseif difference % 1000 == 0 then
          increment = 1000
        elseif difference % 100 == 0 then
          increment = 100
        elseif difference % 10 == 0 then
          increment = 10
        else
          increment = 1
        end
        if current_amount < bar.amount_displayed then
          increment = increment * -1
        end
        bar.amount_displayed = bar.amount_displayed + increment
      end
      -- Redraw the surface only if something has changed.
      if need_rebuild then
        bar:rebuild_surface()
      end
  --print("Magic:", current_amount, "/", max_amount, "  || Displayed:", bar.amount_displayed)

      sol.timer.start(game, CHECK_FREQUENCY, function() bar:check() end)

    end


    function bar:rebuild_surface()
      local max_amount = max_amount_function()
      bar.surface:clear()
      bar.background:draw_region(0, 0, max_amount / DRAW_RATIO, 8, bar.surface, 0, 0)
      bar.foreground:draw_region(0, 0, bar.amount_displayed / DRAW_RATIO, 8, bar.surface, 0, 0)
      bar.endcap:draw(bar.surface, (max_amount / DRAW_RATIO - 8), 0)
    end


    function bar:on_draw(dst_surface)
      local x, y = bar.dst_x, bar.dst_y
      local width, height = dst_surface:get_size()
      if x < 0 then
        x = width + x
      end
      if y < 0 then
        y = height + y
      end
      bar.surface:draw(dst_surface, x, y)
    end


    function bar:on_started()
      bar:check()
      bar:rebuild_surface()
    end


    function bar:set_color(new_color)
      bar.color = new_color
      bar.foreground = sol.sprite.create("hud/stat_bars/" .. new_color)
      bar:rebuild_surface()
    end

    if fade_on_suspend then
      function bar:on_paused()
        bar.surface:fade_out()
      end
      function bar:on_unpaused()
        bar.surface:fade_in()
      end
    end

    return bar
  end

  return bar_builder
end

return builder_builder