local possible_items = require("items/materials/materials_manager"):get_all_materials()

local menu = require("scripts/menus/inventory/bottomless_list"):build{
  all_items = possible_items,
  num_columns = 3,
  num_rows = 3,
  menu_x = 16
}


menu:register_event("on_command_pressed", function(self, cmd)
  local game = sol.main.get_game()
  if cmd == "action" then
    local item = menu:get_current_item()
    if item.on_using then
      item:on_using()
      menu:rebuild_items()
    end
  end
end)

menu:register_event("on_started", function()
  sol.main.get_game():set_suspended(true)
end)

menu:register_event("on_finished", function()
  sol.main.get_game():set_suspended(false)
end)


return menu