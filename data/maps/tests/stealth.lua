local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  for e in map:get_entities_by_type("enemy") do
    e.idle_movement_speed = 50
  end
end)
