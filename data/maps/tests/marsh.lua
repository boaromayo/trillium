local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({170,180,200})
  map:set_fog("fog")
end)